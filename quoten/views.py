from django.shortcuts import render
from django.views.generic.detail import DetailView
from quoten.models import Initiative

# Create your views here.

class InitiaveView(DetailView):
    model = Initiative
    context_object_name = "initiative"

