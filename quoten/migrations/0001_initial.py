# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-18 19:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Initiative',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('goal', models.SmallIntegerField()),
                ('start', models.DateField()),
                ('end', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('order', models.SmallIntegerField()),
                ('goal', models.SmallIntegerField()),
                ('amount', models.SmallIntegerField()),
                ('initiative', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='quoten.Initiative')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
