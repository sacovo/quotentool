'''
Custom filters and tags for quoten
'''
import datetime
import urllib.request
import plotly.offline as py
import plotly.graph_objs as go

from django.utils.safestring import mark_safe
from django import template
from django.template.defaultfilters import stringfilter
register = template.Library()

@register.simple_tag
def overview(initiative):
    collected = initiative.collected()
    soll = int(initiative.needed_at_date(datetime.date.today()))
    c_both = "rgba(49, 193, 83, 1)"
    c_plus = "rgba(79, 255, 87, 1)"
    c_minus = "rgba(189, 56, 55, 1)"
    c_rest = "rgba(169, 167, 167, 1)"
    if collected < soll:
        c1 = c_both
        c2 = c_minus
        text = "{0} Unterschriften Rückstand".format(soll-collected)
    else:
        c1 = c_plus
        c2 = c_both
        text = "{0} Unterschriften Vorsrpung".format(collected-soll)
    trace1 = go.Bar(
        y=[initiative.name],
        x=[collected],
        name="Gesammelt",
        orientation = 'h',
        marker = dict(
            color=c1
        )
    )
    trace2 = go.Bar(
        y=[initiative.name],
        x=[soll],
        name="Benötigt",
        orientation = 'h',
        marker = dict(
            color=c2
        )
    )
    trace3 = go.Bar(
        y=[initiative.name],
        x=[initiative.goal],
        name="Total",
        orientation = 'h',
        marker = dict(
            color=c_rest
        )
    )

    if soll > collected:
        data = [trace3, trace2, trace1]
    else:
        data = [trace3, trace1, trace2]

    layout = go.Layout(
        barmode="overlay",
        title=initiative.name,
        showlegend=False,
        xaxis=dict(
            range=[0, initiative.goal]
            ),
        yaxis=dict(
            showgrid=False,
            zeroline=False,
            showline=False,
            autotick=True,
            ticks='',
            showticklabels=False
            ),
        margin = {
            'l' : 140,
        },
        annotations=[
            dict(
                x=max(soll, collected),
                y=initiative.name,
                xref='x',
                yref='y',
                text=text,
                showarrow=False,
                ax=0,
                ay=-40
                )
            ]


        )
    fig = go.Figure(data=data, layout=layout)
    return mark_safe(py.plot(fig, include_plotlyjs=False, output_type="div"))
