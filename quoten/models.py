from django.db import models

# Create your models here.

class Initiative(models.Model):
    name = models.CharField(max_length=250)
    goal = models.SmallIntegerField()
    start = models.DateField()
    end = models.DateField()

    def collected(self):
        quotas = self.quote_set.all()
        return sum((q.amount for q in quotas))

    def needed_at_date(self, date):
        '''
        Returns the total amount * (date - start)/(end - start)
        '''
        return self.goal * self.percentage_at_date(date)

    def percentage_at_date(self, date):
        '''
        Returns the (date - start)/(end - start)
        '''
        return (date - self.start)/(self.end - self.start)

    def percentage(self):
        return self.collected()/self.goal


class Quote(models.Model):
    initiative = models.ForeignKey(Initiative)
    name = models.CharField(max_length=200)
    order = models.SmallIntegerField()
    goal = models.SmallIntegerField()
    amount = models.SmallIntegerField()

    class Meta:
        ordering = ['order']

    def needed_at_date(self, date):
        '''
        Returns the total amount * (date - start)/(end - start)
        '''
        return self.goal * self.initiative.percentage_at_date(date)

    def percentage(self):
        return self.amount/self.goal

    def collected(self):
        return self.amount
