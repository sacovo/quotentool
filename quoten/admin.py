from django.contrib import admin
from quoten.models import Initiative, Quote

class QuoteAdmin(admin.TabularInline):
    model = Quote
    extra = 1
    list_display = ['name', 'goal', 'amount', 'order']

# Register your models here.
@admin.register(Initiative)
class InitiativeAdmin(admin.ModelAdmin):
    list_display = ['name', 'start', 'end', 'goal', 'collected']
    inlines = [
        QuoteAdmin,
    ]

